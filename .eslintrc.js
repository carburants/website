module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb', 'prettier', 'plugin:flowtype/recommended'],
  plugins: ['react', 'jsx-a11y', 'import', 'flowtype'],
  rules: {
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'no-underscore-dangle': ["error", { "allow": ["_ne", "_sw", "_id"] }],
    "react/jsx-props-no-spreading": [1, {
      "exceptions": ["ReactMapGL"]
    }]
  },
  env: {
    browser: true,
    es6: true,
  }
};
